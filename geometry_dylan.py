from point_dylan import Point
from linesegment import LineSegment
import math

class Rectangle:
    def __init__(self, x, y, w, h):
        self.p1 = Point(x, y)
        self.p2 = Point(x + w, y)
        self.p4 = Point(x, y + h)
        self.p3 = Point(x + w, y + h)
    
    def sides(self):
        self.a = LineSegment(self.p1, self.p2).length()
        self.b = LineSegment(self.p2, self.p3).length()
        self.c = LineSegment(self.p3, self.p4).length()
        self.d = LineSegment(self.p4, self.p1).length()
        return self.a, self.b, self.c, self.d

    def perimeter(self):
        return 2*(self.a + self.b)

    def area(self):
        return self.a * self.b

    def corners(self):
        return self.p1, self.p2, self.p3, self.p4

    def diagonals(self):
        return LineSegment(self.p1, self.p3), LineSegment(self.p2, self.p4)  

class Circle:
    def __init__(self, radius):
        self.radius = radius

    def diameter(self):
        return self.radius * 2

    def circumference(self):
        return 2 * math.pi * self.radius

    def area(self):
        return math.pi * self.radius ** 2
from point_dylan import Point

class LineSegment:
    def __init__(self, p1, p2):
        self.p1 = p1
        self.p2 = p2

    def length(self):
        math = __import__('math')
        return math.sqrt((self.p1.x - self.p2.x) ** 2 + (self.p1.y - self.p2.y) ** 2)

    def slope(self):
        return (p2.y - p1.y) / (p2.x - p1.x)

    def y_intercept(self):
        slope = self.slope()
        return -1*p1.x*slope+p1.y

    def x_intercept(self):
        y_int = self.y_intercept()
        slope = self.slope()
        return (0 - y_int) / slope

    def midpoint(self):
        return ((p1.x + p2.x)/2, (p1.y + p2.y)/2)

if __name__ == '__main__':
    p1 = (1, 1)
    p2 = 2, 4
    segment = LineSegment(p1, p2)
    print(segment.length())
    print(segment.slope())
    print(segment.y_intercept())
    print(segment.x_intercept())
    print(segment.midpoint())
