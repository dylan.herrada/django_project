from point_dylan import Point

class LineSegment:
    def __init__(self, p1, p2):
        self.p1 = p1
        self.p2 = p2

    def length(self):
        math = __import__('math')
        return math.sqrt((self.p1.x - self.p2.x) ** 2 + (self.p1.y - self.p2.y) ** 2)

    def slope(self):
        return (p2.y - p1.y) / (p2.x - p1.x)

    def y_intercept(self):
        slope = self.slope()
        return -1*p1.x*slope+p1.y

    def x_intercept(self):
        y_int = self.y_intercept()
        slope = self.slope()
        return (0 - y_int) / slope

    def midpoint(self):
        return ((p1.x + p2.x)/2, (p1.y + p2.y)/2)

if __name__ == '__main__':
    p1 = (1, 1)
    p2 = 2, 4
    segment = LineSegment(p1, p2)
    print(segment.length())
    print(segment.slope())
    print(segment.y_intercept())
    print(segment.x_intercept())
    print(segment.midpoint())
