class Polynomial:
    """
      >>> p1 = Polynomial((4, 0, 2, 3))
      >>> print(p1)
      4x^3 + 2x + 3
      >>> p1
      Polynomial((4, 0, 2, 3))
    """
    def __init__(self, polynomial):
        self.p = polynomial
    
    def __repr__(self):
        return f'Polynomial({self.p})'

    def __str__(self):
        poly = ''
        if type(self.p) == int:
            return str(self.p) 
        for i in range(len(self.p)):
            if i > 0 and i < len(self.p) and self.p[i] > 1:
                poly += ' + ' + str(abs(self.p[i]))
            elif self.p[i] < 0:
                poly += ' - ' + str(abs(p[i]))
            elif self.p[i] and self.p[i] > 1:
                poly += str(abs(self.p[i]))
            if len(self.p) - i > 1 and self.p[i]:
                poly += 'x'
            if len(self.p) - i > 2  and self.p[i]:
                poly += '^' + str(len(self.p) - (i + 1))
        return poly

if __name__ == '__main__':
    import doctest
    doctest.testmod()
