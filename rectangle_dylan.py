from point_dylan import Point
from linesegment import LineSegment
import math

class Rectangle:
    def __init__(self, x, y, w, h):
        self.p1 = Point(x, y)
        self.p2 = Point(x + w, y)
        self.p4 = Point(x, y + h)
        self.p3 = Point(x + w, y + h)
    
    def sides(self):
        self.a = LineSegment(self.p1, self.p2).length()
        self.b = LineSegment(self.p2, self.p3).length()
        self.c = LineSegment(self.p3, self.p4).length()
        self.d = LineSegment(self.p4, self.p1).length()
        return self.a, self.b, self.c, self.d

    def perimeter(self):
        return 2*(self.a + self.b)

    def area(self):
        return self.a * self.b

    def corners(self):
        return self.p1, self.p2, self.p3, self.p4

    def diagonals(self):
        return LineSegment(self.p1, self.p3), LineSegment(self.p2, self.p4)  

if __name__ == '__main__':
    rectangle = Rectangle(3, 4, 5, 6)
    print(rectangle.sides())
    print('\n')
    print(rectangle.perimeter())
    print('\n')
    print(rectangle.area())
    print('\n')
    print(rectangle.corners())
    print('\n')
    print(rectangle.diagonals())
