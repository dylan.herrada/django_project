class Poly:
    def __init__(self, poly):
        self.tp = poly
        self.sp = self.write_poly()

    def write_poly(self):
        poly_str = ''

        if len(self.tp) == 1:
            return str(self.tp[0])
        poly_str = f'{self.tp[-2]}x'
        if self.tp[-1]:
            poly_str += f' + {self.tp[-1]}'
        for degree in range(2, len(self.tp)):    
            if self.tp[-(degree+1)] == 0:
                continue
            poly_str = f'{self.tp[-(degree+1)]}x^{degree} + ' + poly_str

        self.sp = poly_str

    def read_term(term_str):
        """
          >>> read_term('4x^5')
          (4, 5)
          >>> read_term('7x')
          (7, 1)
          >>> read_term('8.5')
          (8.5, 0)
        """
        if 'x^' in term_str:
            coeff_str = term_str[:term_str.find('x')]
            coeff = float(coeff_str) if '.' in coeff_str else int(coeff_str)
            degree = int(term_str[term_str.find('^')+1:])
            return (coeff, degree)
        if 'x' in term_str:
            coeff = float(term_str[:-1]) if '.' in term_str else int(term_str[:-1])
            return (coeff, 1)
        return (float(term_str) if '.' in term_str else int(term_str), 0)

    def read_poly(poly_str):
        """
          >>> read_poly('-2x^3 + 4x + -5')
          [-2, 0, 4, -5]
          >>> read_poly('2x^3 + 3x^2 + 4x + 5')
          [2, 3, 4, 5]
        """
        terms = poly_str.split(' + ')
        if '^' in terms[0]:
            degree = int(terms[0][terms[0].find('^')+1:])
        elif 'x' in terms[0]:
            degree = 1                 # it's a linear polynomial
        else:
            degree = 0                 # it's a constant polynomial
        poly = (degree + 1) * [0]
        for term_str in terms:
            coeff, exp = read_term(term_str)
            poly[degree-exp] = coeff
        return poly
