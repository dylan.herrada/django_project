class Point:
    """Represents a point in 2-D space."""

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
        

    def __repr__(self):
        return f'Point({self.x}, {self.y})'

    def __str__(self):
        return f'({self.x}, {self.y})'

    def dist_from_origin(self):
        return (self.x ** 2 + self.y ** 2) ** 0.5

    def distance(self, point):
        math = __import__('math')
        return math.sqrt((point[0] - self.x) ** 2 + (point[1] - self.y) ** 2)

if __name__ == '__main__':
    p1 = Point()
    print(p1.distance((10, 10)))
