import math

class Circle:
    def __init__(self, radius):
        self.radius = radius

    def diameter(self):
        return self.radius * 2

    def circumference(self):
        return 2 * math.pi * self.radius

    def area(self):
        return math.pi * self.radius ** 2

if __name__ == '__main__':
    c = Circle(5)
    print(c.diameter())
    print(c.circumference())
    print(c.area())
