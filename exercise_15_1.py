class Circle:
    def __init__(self):
        self.center = (100, 150)
        self.radius = 75

    def point_in_circle(self, point):
        if ((point[0] - self.center[0])**2 + (point[1] - self.center[1]) ** 2) <= self.radius ** 2:
            return True
        else:
            return False

    def rect_in_circle(self, rectangle):
        for i in rectangle:
            if not self.point_in_circle(i):
                return False

        return True

    def rect_circle_overlap(self, rectangle):
        for i in rectangle:
            if self.point_in_circle(i):
                return True

        return False

p1 = Circle()
print(p1.point_in_circle((101, 151)))

print(p1.rect_in_circle(((99, 151), (101, 151), (99, 149), (101, 149))))

print(p1.rect_circle_overlap(((100, 150), (180, 150), (100, 250), (180, 250))))
