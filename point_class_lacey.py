class Point:
    """Represents a point in 2d space with x and y coordinates"""
    def print_point(p):
        print(f'{((p.x), (p.y))}')



class Rectangle:
    """Represents a rectangle w attributes width, height, and corner"""
    box = Rectangle()
    box.width = 100.0
    box.height = 200.0
    box.corner = Point()
    box.corner.x = 0.0
    box.corner.y = 0.0
    def find_center(rect):
        p = Point()
        p.x = rect.corner.x + rect.width/2
        p.y = rect.corner.y + rect.height/2
        return p
    def grow_rectangle(rect, dwidth, dheight):
        rect.width += dwidth
        rect.height += dheight
    
        
