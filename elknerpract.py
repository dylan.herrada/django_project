class Point:
    """Represents a point in 2-D space.

      >>> p = Point(3, 4)
    """

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def __repr__(self):
        """
          >>> p = Point(3, 4)
          >>> p
          Point(3, 4)
        """
        return f'Point({self.x}, {self.y})'

    def __str__(self):
        """
          >>> p = Point(3, 4)
          >>> print(p)
          (3, 4)
        """
        return f'({self.x}, {self.y})'

    def dist_from_origin(self):
        """
          >>> p = Point(3, 4)
          >>> p.dist_from_origin()
          5.0
        """
        return (self.x ** 2 + self.y ** 2) ** 0.5

    def distance(self, other):
        """
          >>> p1 = Point(1, 1)
          >>> p2 = Point(4, 5)
          >>> p1.distance(p2)
          5.0
        """
        return ((self.x - other.x) ** 2 + (self.y - other.y) ** 2) ** 0.5


class LineSegment:
    """Represents a line segment in 2-D space using two Point objects.

      >>> p1 = Point(1, 1)
      >>> p2 = Point(4, 5)
      >>> ls = LineSegment(p1, p2)
    """
    def __init__(self, p1, p2):
        self.endp1 = p1
        self.endp2 = p2

    def length(self):
        """
          >>> ls = LineSegment(Point(1, 1), Point(4, 5))
          >>> ls.length()
          5.0
        """
        return self.endp1.distance(self.endp2)

    def slope(self):
        """Return the slope of the line containing the line segment, or
        NoSlopeError if slope is undefined.

          >>> ls = LineSegment(Point(1, 1), Point(4, 5))
          >>> round(ls.slope(), 2)
          1.33
        """
        return (self.endp1.y - self.endp2.y) / (self.endp1.x - self.endp2.x)

    def y_intercept(self):
        """Return the y int of the line containing the linesegment
          >>> ls = LineSegment(Point(1, 1), Point(5, 5))
          >>> ls.y_intercept()
          Point(0.0, 0)
        """
        slope = self.slope()
        return Point(self.endp1.y - (slope * self.endp1.x), 0) 

class Rectangle:
    """Represents a rectangle in 2D space
    """
    def __init__(self, p1, height, width):
        self.p1 = p1
        self.height = height
        self.width = width

    def perimeter(self):
        return (2 * self.height) + (2 * self.width)

    def area(self):
        return self.height * self.width

    def corners(self):
        p2 = Point(p1.x + self.width, p1.y + self.height)
        p3 = Point(p1.x + self.width, p1.y)
        p4 = Point(p1.x, p1.y + self.height)
        return self.p1, p2, p3, p4

if  __name__ == '__main__':
    import doctest
    doctest.testmod()

    ls = LineSegment(Point(1, 1), Point(5, 5))
    print(round(ls.slope(), 2))
    print(ls.y_intercept())
