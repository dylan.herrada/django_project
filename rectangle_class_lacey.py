class Rectangle:
    def __init__(self, p1, height, width):
        self.p1 = p1
        self.height = height
        self.width = width

    def perimeter(self):
        return (2 * self.height) + (2 * self.width)

    def area(self):
        return self.height * self.width

    def corners(self):
        p2 = Point(p1.x + self.width, p1.y + self.height)  
        p3 = Point(p1.x + self.width, p1.y)
        p4 = Point(p1.x, p1.y + self.height)
        return self.p1, p2, p3, p4

    def sides(self):
        """Return line segment instances that represent the side of thei
        rectanlge
        """
        c1, c2, c3, c4 = self.corners()
        side1 = LineSegment(c1, c3)
        side2 = LineSegment(c1, c4)
        side3 = LineSegment(c2, c3)
        side4 = LineSegment(c2, c4)
        return side1, side2, side3, side4
    
    def diagonals(self):
        c1, c2, c3, c4, = self.corners()
        diagonal1 = LineSegment(c1, c2)
        diagonal2 = LineSegment(c4, c3)
        return diagonal1, diagonal2


from line_segment_lacey import LineSegment
from point_class_lacey import Point
p1 = Point(0, 0)
height = 4
width = 2
myrect = Rectangle(p1, height, width)
print(myrect.perimeter())
print(myrect.area())
print(myrect.corners())
print(myrect.sides())
print(myrect.diagonals())
