class LineSegment:
    def __init__(self, p1, p2):
        """takes two line segment endpoints as attributes"""
        self.p1 = p1
        self.p2 = p2
    
    def length(self):
        return ((p2.x - p1.x) ** 2 + (p2.y - p1.y) ** 2) ** 0.5

    def slope(self):
        return (p2.y - p1.y) / (p2.x - p1.x)

    def y_intercept(self):
        slope = self.slope()
        return -1 * p1.x * slope + p1.y 

    def x_intercept(self):
        y_intercept = self.y_intercept()
        slope = self.slope()
        return (p1.y - y_intercept) / slope

    def midpoint(self):
        return (((p1.x + p2.x) / 2), ((p1.y + p2.y) / 2))

from point_class_lacey import Point
p1 = Point(4, 5)
p2 = Point(3, 8)
line = LineSegment(p1, p2)
print(line.length())
print(line.slope())
print(line.y_intercept())
print(line.x_intercept())
print(line.midpoint())
